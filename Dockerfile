# Builder Image
FROM golang:1.16 AS builder

WORKDIR /usr/src

COPY ./go.mod ./go.sum ./

COPY ./mongodemo/mongodemo_client/go.mod ./mongodemo/mongodemo_client/go.sum ./mongodemo/mongodemo_client/

RUN go mod download 

COPY . .

RUN CGO_ENABLED=0 go build -o /go/bin/go-microservice-server

# Final image
FROM scratch
COPY ./config.yaml /etc/irtsb/
COPY --from=builder /go/bin/go-microservice-server /go/bin/go-microservice-server

ENTRYPOINT ["/go/bin/go-microservice-server"]

