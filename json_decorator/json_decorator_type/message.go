package json_decorator_type

type Message struct {
	Message string `json:"message" binding:"required"`
}
