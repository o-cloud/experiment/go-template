package json_decorator

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/irt-sb1/go-template/json_decorator/json_decorator_type"

	"github.com/gin-gonic/gin"
)

func getRouter() *gin.Engine {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	JSONDecoratorHandler{}.SetupRoutes(r.Group("/default"))
	return r
}

func TestHandleDecorate(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/decorate", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("message", "le test")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	getRouter().ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expectedBody := `{"message":"le test"}`
	if rr.Body.String() != expectedBody {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedBody)
	}
}

func TestHandleDecorateInvalidMessage(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/decorate", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	getRouter().ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestHandleExtract(t *testing.T) {

	messageBody := json_decorator_type.Message{}
	messageBody.Message = "le test a sortir"
	// Insert URL query
	b, _ := json.Marshal(messageBody)

	req, err := http.NewRequest("POST", "/default/extract", bytes.NewBuffer(b))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	getRouter().ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expectedBody := `le test a sortir`
	if rr.Body.String() != expectedBody {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedBody)
	}
}

func TestHandleExtractWrongBody(t *testing.T) {

	// Insert URL query
	b, _ := json.Marshal("")

	req, err := http.NewRequest("POST", "/default/extract", bytes.NewBuffer(b))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	getRouter().ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}
