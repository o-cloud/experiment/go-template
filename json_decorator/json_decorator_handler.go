package json_decorator

import (
	"fmt"
	"net/http"

	"gitlab.com/irt-sb1/go-template/json_decorator/json_decorator_type"

	"github.com/gin-gonic/gin"
)

type JSONDecoratorHandler struct {
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (C JSONDecoratorHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/decorate", decorateHandler)
	router.POST("/extract", extractHandler)
}

// decorateHandler read the "message" parameter in the query and return a JSON with the content in the "message" key
func decorateHandler(c *gin.Context) {
	m, ok := c.GetQuery("message")
	if !ok {
		_ = c.Error(fmt.Errorf("parameter 'message' is not defined"))
		c.Status(http.StatusBadRequest)
		return
	}
	c.JSON(http.StatusOK, decorate(m))
}

// extractHandler get the JSON content of the request and return the message inside
func extractHandler(c *gin.Context) {
	var message json_decorator_type.Message
	if err := c.BindJSON(&message); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, extract(message))
}
