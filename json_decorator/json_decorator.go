// Package json_decorator contains functions to do operations on multiple numbers
package json_decorator

import "gitlab.com/irt-sb1/go-template/json_decorator/json_decorator_type"

func decorate(m string) json_decorator_type.Message {
	return json_decorator_type.Message{
		Message: m,
	}
}

func extract(message json_decorator_type.Message) string {
	return message.Message
}
