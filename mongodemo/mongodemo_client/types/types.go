package types

type Message struct {
	Id      string `json:"id"`
	Titre   string `json:"titre" binding:"required"`
	Contenu string `json:"contenu" binding:"required"`
}

type IMongoDemo interface {
	GetAllMessages() ([]Message, error)
	InsertMessage(m *Message) error
	GetMessage(id string) (Message, error)
	UpdateMessage(m *Message) error
	DeleteMessage(id string) error
}
