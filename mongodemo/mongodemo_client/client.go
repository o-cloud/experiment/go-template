package mongodemo_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	mdtype "gitlab.com/irt-sb1/go-template/mongodemo/mongodemo_client/types"
)

type ClientMongoDemo struct {
	baseURL string
	client  *http.Client
}

func NewClient(url string) mdtype.IMongoDemo {
	return &ClientMongoDemo{
		baseURL: url,
		client:  &http.Client{},
	}
}

func (c *ClientMongoDemo) GetAllMessages() ([]mdtype.Message, error) {
	bod, err := c.doGet(fmt.Sprintf("%s/message", c.baseURL))
	if err != nil {
		return nil, err
	}

	retArray := make([]mdtype.Message, 0)
	json.Unmarshal(bod, &retArray)
	return retArray, nil
}

func (c *ClientMongoDemo) InsertMessage(m *mdtype.Message) error {
	url := fmt.Sprintf("%s/message", c.baseURL)
	data, err := json.Marshal(m)
	if err != nil {
		return err
	}
	_, err = c.doPut(url, data)
	return err
}

func (c *ClientMongoDemo) GetMessage(id string) (mdtype.Message, error) {
	var ret mdtype.Message
	bod, err := c.doGet(fmt.Sprintf("%s/message/%s", c.baseURL, id))
	if err != nil {
		return ret, err
	}
	err = json.Unmarshal(bod, &ret)
	return ret, err
}

func (c *ClientMongoDemo) UpdateMessage(m *mdtype.Message) error {
	url := fmt.Sprintf("%s/message/%s", c.baseURL, m.Id)
	data, err := json.Marshal(m)
	if err != nil {
		return err
	}
	_, err = c.doPost(url, data)
	return err
}

func (c *ClientMongoDemo) DeleteMessage(id string) error {
	url := fmt.Sprintf("%s/message/%s", c.baseURL, id)
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}
	_, err = c.doRequest(req)
	return err
}

// Helpers functions

func (c *ClientMongoDemo) doRequest(req *http.Request) ([]byte, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode >= 200 || resp.StatusCode < 300 {
		return nil, fmt.Errorf("%s", body)
	}
	return body, nil
}

func (c *ClientMongoDemo) doGet(url string) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	bod, err := c.doRequest(req)
	return bod, err
}

func (c *ClientMongoDemo) doPost(url string, data []byte) ([]byte, error) {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
	bod, err := c.doRequest(req)
	return bod, err
}

func (c *ClientMongoDemo) doPut(url string, data []byte) ([]byte, error) {
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
	bod, err := c.doRequest(req)
	return bod, err
}
