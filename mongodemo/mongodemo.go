package mongodemo

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"time"

	mongodemo_type "gitlab.com/irt-sb1/go-template/mongodemo/mongodemo_client/types"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDemo struct {
	Col *mongo.Collection
}

type MessageBson struct {
	Id      primitive.ObjectID `bson:"_id,omitempty"`
	Titre   string             `bson:"titre,omitempty"`
	Contenu string             `bson:"contenu"`
}

func GetMongoDemo() mongodemo_type.IMongoDemo {
	mongourl := url.URL{
		Scheme: "mongodb",
		Host:   viper.GetString("MONGO_HOST") + ":" + viper.GetString("MONGO_PORT"),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongourl.String()))
	if err != nil {
		panic("Erreur initialisation Client MongoDB")
	}

	return &MongoDemo{
		Col: client.Database("testDB").Collection("message"),
	}
}

//IMongoDemo Implementation

func (md *MongoDemo) GetAllMessages() ([]mongodemo_type.Message, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	results := make([]MessageBson, 0)
	cur, err := md.Col.Find(ctx, bson.D{})
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer cur.Close(ctx)
	cur.All(ctx, &results)
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	return *MessageBsonsToMessages(&results), err
}

func (md *MongoDemo) InsertMessage(m *mongodemo_type.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, errInsert := md.Col.InsertOne(ctx, MessageToMessageBson(m))
	if errInsert != nil {
		log.Fatal(errInsert)
	}
	return errInsert
}

func (md *MongoDemo) GetMessage(id string) (mongodemo_type.Message, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var result MessageBson
	bsonid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return mongodemo_type.Message{}, err
	}
	err = md.Col.FindOne(ctx, bson.D{{"_id", bsonid}}).Decode(&result)
	fmt.Println(result)
	if err == mongo.ErrNoDocuments {
		// Do something when no record was found
		fmt.Println("record does not exist")
	}
	return *MessageBsonToMessage(&result), err
}

func (md *MongoDemo) UpdateMessage(m *mongodemo_type.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	mBson := *MessageToMessageBson(m)
	marshalledBson, err := bson.Marshal(mBson)
	if err != nil {
		log.Fatal(err)
		return err
	}
	_, errInsert := md.Col.ReplaceOne(ctx, bson.D{{"_id", mBson.Id}}, marshalledBson)
	return errInsert
}

func (md *MongoDemo) DeleteMessage(id string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Fatal(err)
		return err
	}
	filter := bson.D{{"_id", objectId}}
	_, err = md.Col.DeleteOne(ctx, filter)
	return err
}

//Mapping functions

func MessageToMessageBson(m *mongodemo_type.Message) *MessageBson {
	message := &MessageBson{Titre: m.Titre, Contenu: m.Contenu}
	if primid, ok := primitive.ObjectIDFromHex(m.Id); ok == nil {
		message.Id = primid
	}
	return message
}

func MessagesToMessageBsons(m *[]mongodemo_type.Message) *[]MessageBson {
	retArray := make([]MessageBson, len(*m))
	for i, v := range *m {
		retArray[i] = *MessageToMessageBson(&v)
	}
	return &retArray
}

func MessageBsonToMessage(m *MessageBson) *mongodemo_type.Message {
	return &mongodemo_type.Message{Id: m.Id.Hex(), Titre: m.Titre, Contenu: m.Contenu}
}

func MessageBsonsToMessages(m *[]MessageBson) *[]mongodemo_type.Message {
	retArray := make([]mongodemo_type.Message, len(*m))
	for i, v := range *m {
		retArray[i] = *MessageBsonToMessage(&v)
	}
	return &retArray
}
