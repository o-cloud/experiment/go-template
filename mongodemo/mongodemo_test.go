package mongodemo

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	mongodemo_type "gitlab.com/irt-sb1/go-template/mongodemo/mongodemo_client/types"

	"github.com/gin-gonic/gin"
)

type testSetup struct {
	t      *testing.T
	engine *gin.Engine
	mmd    *MockMongoDemo
	req    *http.Request
	rr     *httptest.ResponseRecorder
}

func getSetup(t *testing.T, method, url string, body interface{}) testSetup {

	req, err := http.NewRequest(method, url, nil)
	if body != nil {
		byts, _ := json.Marshal(body)
		req, err = http.NewRequest(method, url, bytes.NewBuffer(byts))
	}
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	r := gin.New()
	mmd := &MockMongoDemo{}
	(&MongoDemoHandler{MD: mmd}).SetupRoutes(r.Group("/default"))
	return testSetup{
		t:      t,
		engine: r,
		mmd:    mmd,
		req:    req,
		rr:     rr,
	}
}

func TestHandleGetAllMessages(t *testing.T) {
	test := getSetup(t, "GET", "/default/message", nil)
	test.mmd.retArray = make([]mongodemo_type.Message, 1)
	test.mmd.retArray[0] = mongodemo_type.Message{Id: "identifiant", Titre: "titre", Contenu: "contenu"}
	test.run()
	test.expectMethodCall("GetAllMessages()")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`[{"id":"identifiant","titre":"titre","contenu":"contenu"}]`)
}

func TestHandleGetAllMessagesErreur(t *testing.T) {
	test := getSetup(t, "GET", "/default/message", nil)
	test.mmd.err = errors.New("Erreur")
	test.run()
	test.expectMethodCall("GetAllMessages()")
	test.expectResponseStatus(http.StatusInternalServerError)
	test.expectBodyContent("")
}

func TestHandleInsertMessageErrorBody(t *testing.T) {
	test := getSetup(t, "PUT", "/default/message", "plop")
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusBadRequest)
	test.expectBodyContent("")
}

func TestHandleInsertMessageError(t *testing.T) {
	m := mongodemo_type.Message{Id: "identifiant", Titre: "titre", Contenu: "contenu"}
	test := getSetup(t, "PUT", "/default/message", m)
	test.mmd.err = errors.New("Erreur")
	test.run()
	test.expectMethodCall(`InsertMessage(&{identifiant titre contenu})`)
	test.expectResponseStatus(http.StatusInternalServerError)
	test.expectBodyContent("")
}

func TestHandleInsertMessage(t *testing.T) {
	m := mongodemo_type.Message{Id: "identifiant", Titre: "titre", Contenu: "contenu"}
	test := getSetup(t, "PUT", "/default/message", m)
	test.run()
	test.expectMethodCall(`InsertMessage(&{identifiant titre contenu})`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent("")
}

func TestHandleGetMessage(t *testing.T) {
	m := mongodemo_type.Message{Id: "identifiant", Titre: "titre", Contenu: "contenu"}
	test := getSetup(t, "GET", "/default/message/identifiant", nil)
	test.mmd.retArray = []mongodemo_type.Message{m}
	test.run()
	test.expectMethodCall(`GetMessage(identifiant)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"id":"identifiant","titre":"titre","contenu":"contenu"}`)
}

func TestHandleGetMessageError(t *testing.T) {
	test := getSetup(t, "GET", "/default/message/randomkey", nil)
	test.mmd.retArray = make([]mongodemo_type.Message, 1)
	test.mmd.err = errors.New("erreur")
	test.run()
	test.expectMethodCall(`GetMessage(randomkey)`)
	test.expectResponseStatus(http.StatusInternalServerError)
	test.expectBodyContent(``)
}

//Update

func TestHandleUpdateMessageErrorBody(t *testing.T) {
	test := getSetup(t, "POST", "/default/message", "plop")
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusBadRequest)
	test.expectBodyContent("")
}

func TestHandleUpdateMessageError(t *testing.T) {
	m := mongodemo_type.Message{Id: "identifiant", Titre: "titre", Contenu: "contenu"}
	test := getSetup(t, "POST", "/default/message", m)
	test.mmd.err = errors.New("Erreur")
	test.run()
	test.expectMethodCall(`UpdateMessage(&{identifiant titre contenu})`)
	test.expectResponseStatus(http.StatusInternalServerError)
	test.expectBodyContent("")
}

func TestHandleUpdateMessage(t *testing.T) {
	m := mongodemo_type.Message{Id: "identifiant", Titre: "titre", Contenu: "contenu"}
	test := getSetup(t, "POST", "/default/message", m)
	test.run()
	test.expectMethodCall(`UpdateMessage(&{identifiant titre contenu})`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent("")
}

func TestHandleDeleteMessage(t *testing.T) {
	m := mongodemo_type.Message{Id: "identifiant", Titre: "titre", Contenu: "contenu"}
	test := getSetup(t, "DELETE", "/default/message/identifiant", nil)
	test.mmd.retArray = []mongodemo_type.Message{m}
	test.run()
	test.expectMethodCall(`DeleteMessage(identifiant)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(``)
}

func TestHandleDeleteMessageError(t *testing.T) {
	test := getSetup(t, "DELETE", "/default/message/randomkey", nil)
	test.mmd.retArray = make([]mongodemo_type.Message, 1)
	test.mmd.err = errors.New("erreur")
	test.run()
	test.expectMethodCall(`DeleteMessage(randomkey)`)
	test.expectResponseStatus(http.StatusInternalServerError)
	test.expectBodyContent(``)
}

//Helper func
func (ts *testSetup) run() {
	ts.engine.ServeHTTP(ts.rr, ts.req)
}

// Check the response code is what we expect.
func (ts *testSetup) expectResponseStatus(expectedCode int) {
	if ts.rr.Code != expectedCode {
		ts.t.Errorf("handler returned wrong status code: got %v want %v",
			ts.rr.Code, expectedCode)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectBodyContent(expectedBody string) {
	if ts.rr.Body.String() != expectedBody {
		ts.t.Errorf("handler returned unexpected body: got %v want %v",
			ts.rr.Body.String(), expectedBody)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMethodCall(expectedMethod string) {
	if ts.mmd.called != expectedMethod {
		ts.t.Errorf("expected method not called: got %v want %v",
			ts.mmd.called, expectedMethod)
	}
}

//Mock calc
type MockMongoDemo struct {
	args     string
	called   string
	retArray []mongodemo_type.Message
	err      error
}

func (mmd *MockMongoDemo) GetAllMessages() ([]mongodemo_type.Message, error) {
	mmd.called = mmd.called + "GetAllMessages()"
	return mmd.retArray, mmd.err
}
func (mmd *MockMongoDemo) InsertMessage(m *mongodemo_type.Message) error {
	mmd.called = mmd.called + fmt.Sprintf("InsertMessage(%s)", m)
	return mmd.err
}
func (mmd *MockMongoDemo) GetMessage(id string) (mongodemo_type.Message, error) {
	mmd.called = mmd.called + fmt.Sprintf("GetMessage(%s)", id)
	return mmd.retArray[0], mmd.err
}
func (mmd *MockMongoDemo) UpdateMessage(m *mongodemo_type.Message) error {
	mmd.called = mmd.called + fmt.Sprintf("UpdateMessage(%s)", m)
	return mmd.err
}
func (mmd *MockMongoDemo) DeleteMessage(id string) error {
	mmd.called = mmd.called + fmt.Sprintf("DeleteMessage(%s)", id)
	return mmd.err
}

func (mmd *MockMongoDemo) exceptCalled(expectedFuncCall string, t *testing.T) {

	if mmd.called != expectedFuncCall {
		t.Errorf("Excpected methods to be called: got %v want %v",
			mmd.called, expectedFuncCall)
	}
}

func (mmd *MockMongoDemo) createMessage() {

}
