package mongodemo

import (
	"net/http"

	mongodemo_type "gitlab.com/irt-sb1/go-template/mongodemo/mongodemo_client/types"

	"github.com/gin-gonic/gin"
)

type MongoDemoHandler struct {
	MD mongodemo_type.IMongoDemo
}

func NewMongoDemoHandler() *MongoDemoHandler {
	return &MongoDemoHandler{
		MD: GetMongoDemo(),
	}
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (C *MongoDemoHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/message", C.getAllMessagesHandler)
	router.PUT("/message", C.insertMessageHandler)
	router.GET("/message/:id", C.getMessageHandler)
	router.POST("/message", C.updateMessageHandler)
	router.POST("/message/:id", C.updateMessageHandler)
	router.DELETE("/message/:id", C.DeleteMessageHandler)

}

// decorateHandler read the "message" parameter in the query and return a JSON with the content in the "message" key
func (C *MongoDemoHandler) getAllMessagesHandler(c *gin.Context) {

	if results, err := C.MD.GetAllMessages(); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	} else {
		c.JSON(http.StatusOK, results)
	}

}

// decorateHandler read the "message" parameter in the query and return a JSON with the content in the "message" key
func (C *MongoDemoHandler) insertMessageHandler(c *gin.Context) {
	var message mongodemo_type.Message
	if err := c.BindJSON(&message); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	if err := C.MD.InsertMessage(&message); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	} else {
		c.Status(http.StatusOK)
	}
}

// decorateHandler read the "message" parameter in the query and return a JSON with the content in the "message" key
func (C *MongoDemoHandler) getMessageHandler(c *gin.Context) {
	if res, err := C.MD.GetMessage(c.Param("id")); err != nil {
		c.Error(err)
		c.Status(http.StatusInternalServerError)
	} else {
		c.JSON(http.StatusOK, res)
	}
}

// decorateHandler read the "message" parameter in the query and return a JSON with the content in the "message" key
func (C *MongoDemoHandler) updateMessageHandler(c *gin.Context) {
	var message mongodemo_type.Message
	if err := c.BindJSON(&message); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	if err := C.MD.UpdateMessage(&message); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	} else {
		c.Status(http.StatusOK)
	}
}

// decorateHandler read the "message" parameter in the query and return a JSON with the content in the "message" key
func (C *MongoDemoHandler) DeleteMessageHandler(c *gin.Context) {
	if err := C.MD.DeleteMessage(c.Param("id")); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	} else {
		c.Status(http.StatusOK)
	}
}
