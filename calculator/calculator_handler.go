package calculator

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CalculatorHandler struct {
	C ICalculator
}

func NewCalculatorHandler() CalculatorHandler {
	return CalculatorHandler{
		C: &Calculator{},
	}
}

func (C CalculatorHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/add", C.handleAdd)
	router.GET("/sub", C.handleSub)
	router.GET("/mul", C.handleMul)
	router.GET("/div", C.handleDiv)
}

// handleAdd Handle sum. Return the sum of all the "n" query parameter array
func (C CalculatorHandler) handleAdd(c *gin.Context) {
	nums, err := convertParametersToNumber(c, "n")
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, fmt.Sprint(C.C.Add(nums...)))
}

// handleSub Handle substraction
func (C CalculatorHandler) handleSub(c *gin.Context) {
	num, err := convertParametersToNumber(c, "n")
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	subs, err := convertParametersToNumber(c, "s")
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, fmt.Sprint(C.C.Sub(num[0], subs...)))
}

// handleMul Handle multiplication
func (C CalculatorHandler) handleMul(c *gin.Context) {
	nums, err := convertParametersToNumber(c, "n")
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, fmt.Sprint(C.C.Mul(nums...)))
}

// handleDiv Handle divide.
func (C CalculatorHandler) handleDiv(c *gin.Context) {
	num, err := convertParametersToNumber(c, "n")
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	divs, err := convertParametersToNumber(c, "d")
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, fmt.Sprint(C.C.Div(num[0], divs...)))
}

func convertParametersToNumber(c *gin.Context, paramName string) ([]float64, error) {
	n, ok := c.GetQueryArray(paramName)
	if !ok || len(n) < 1 {
		return nil, fmt.Errorf("parameter %s does not contain an array of integer", paramName)
	}
	retArray := make([]float64, len(n))
	for i, v := range n {
		num, err := strconv.ParseFloat(v, 64)
		if err != nil {
			return nil, err
		}
		retArray[i] = num
	}
	return retArray, nil
}
