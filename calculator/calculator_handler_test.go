package calculator

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func getRouter() (*gin.Engine, *MockCalc) {
	r := gin.Default()
	mc := &MockCalc{"", 0}
	CalculatorHandler{mc}.SetupRoutes(r.Group("/default"))
	return r, mc
}

func TestGetCalculatorHandler(t *testing.T) {
	ch := NewCalculatorHandler()
	if ch.C == nil {
		t.Fail()
	}
}

func TestHandleAdd(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/add", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("n", "1")
	q.Add("n", "2")
	q.Add("n", "3")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expectedBody := `9`
	if rr.Body.String() != expectedBody {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedBody)
	}
}
func TestHandleAddBadParams(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/add", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestHandleSub(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/sub", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("n", "10")
	q.Add("s", "2")
	q.Add("s", "3")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expectedBody := `9`
	if rr.Body.String() != expectedBody {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedBody)
	}
}

func TestHandleSubBadParams(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/sub", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("n", "")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}
func TestHandleSubBadParams2(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/sub", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("n", "10")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestHandleMul(t *testing.T) {
	req, err := http.NewRequest("GET", "/default/mul", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("n", "10")
	q.Add("n", "2")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expectedBody := `9`
	if rr.Body.String() != expectedBody {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedBody)
	}
}

func TestHandleMulBadParams(t *testing.T) {
	req, err := http.NewRequest("GET", "/default/mul", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestHandleDiv(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/div", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("n", "40")
	q.Add("d", "2")
	q.Add("d", "2")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expectedBody := `9`
	if rr.Body.String() != expectedBody {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedBody)
	}
}
func TestHandleDivBadParams(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/div", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}
func TestHandleDivBadParams2(t *testing.T) {

	req, err := http.NewRequest("GET", "/default/div", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Insert URL query
	q := req.URL.Query()
	q.Add("n", "1")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	r, mc := getRouter()
	mc.RetValue = 9
	r.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

//Mock calc

type MockCalc struct {
	args     string
	RetValue float64
}

func (Mc *MockCalc) Add(args ...float64) float64 {
	Mc.args = fmt.Sprint(args)
	return Mc.RetValue
}

func (Mc *MockCalc) Sub(arg1 float64, args ...float64) float64 {
	Mc.args = fmt.Sprint(arg1, " ", args)
	return Mc.RetValue
}

func (Mc *MockCalc) Mul(args ...float64) float64 {
	Mc.args = fmt.Sprint(args)
	return Mc.RetValue
}

func (Mc *MockCalc) Div(arg1 float64, args ...float64) float64 {
	Mc.args = fmt.Sprint(arg1, " ", args)
	return Mc.RetValue
}
