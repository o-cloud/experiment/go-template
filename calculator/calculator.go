// Package calculator contains functions to do operations on multiple numbers
package calculator

type ICalculator interface {
	Add(nums ...float64) float64
	Sub(n1 float64, n2 ...float64) float64
	Mul(nums ...float64) float64
	Div(n1 float64, n2 ...float64) float64
}

type Calculator struct {
}

func (c Calculator) Add(nums ...float64) float64 {
	return add(nums...)
}

func (c Calculator) Sub(n1 float64, n2 ...float64) float64 {
	return sub(n1, add(n2...))
}

func (c Calculator) Mul(nums ...float64) float64 {
	return mul(nums...)
}
func (c Calculator) Div(n1 float64, n2 ...float64) float64 {
	return div(n1, mul(n2...))
}

func add(nums ...float64) float64 {
	res := 0.0
	for _, n := range nums {
		res += n
	}
	return res

}

func sub(n1 float64, n2 float64) float64 {
	return n1 - n2
}

func mul(nums ...float64) float64 {
	res := 1.0
	for _, n := range nums {
		res *= n
	}
	return res
}

func div(n1 float64, n2 float64) float64 {
	return n1 / n2
}
