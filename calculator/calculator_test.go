package calculator

import "testing"

func TestAdd(t *testing.T) {
	c := Calculator{}
	got := c.Add(1, 2, 3)
	if got != 6 {
		t.Errorf("add(1,2) = %f; want 3", got)
	}
}

func TestSub(t *testing.T) {
	c := Calculator{}
	got := c.Sub(9, 6)
	if got != 3 {
		t.Errorf("add(1,2) = %f; want 3", got)
	}

}

func TestMul(t *testing.T) {
	c := Calculator{}
	got := c.Mul(2, 3, 4)
	if got != 24 {
		t.Errorf("add(1,2) = %f; want 3", got)
	}
}

func TestDiv(t *testing.T) {
	c := Calculator{}
	got := c.Div(9, 3)
	if got != 3 {
		t.Errorf("add(1,2) = %f; want 3", got)
	}
}
