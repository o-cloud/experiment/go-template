[![pipeline status](http://gitlab.com/irt-sb1/go-template/badges/master/pipeline.svg)](http://gitlab.com/irt-sb1/go-template/commits/master)
[![coverage report](http://gitlab.com/irt-sb1/go-template/badges/master/coverage.svg)](http://gitlab.com/irt-sb1/go-template/commits/master)


# Projet Go Template

## Prérequis
Accès à une base de donnée Mongodb, par défaut accessible sur localhost:27017

Pour lancer la base de donnée Mongodb via docker : 
```cmd
docker run -p 27017:27017 --name mongo-dev mongo:bionic
```

Pour helm et skaffold on considère que les outils de commande sont installé et accessible. De plus pour que helm et/ou skaffold fonctionne, kubectl doit être présent sur la machine (et lié à un cluster)


## Utilisation

Démarrage et lancement avec go
```bash
go build .

./go-template.exe
ou
./go-template
```

Build et lancement docker
```bash
docker build . -t go-template:latest
docker run --rm -p 9090:9090 -e MONGO_HOST=host.docker.internal go-template:latest
```

Lancement avec skaffold
```
skaffold dev
```


## Contenu/Exemples

- [Projet Go Template](#projet-go-template)
  - [Prérequis](#prérequis)
  - [Utilisation](#utilisation)
  - [Contenu/Exemples](#contenuexemples)
  - [Agencement des packages et du code GO](#agencement-des-packages-et-du-code-go)
  - [Configuration Viper](#configuration-viper)
    - [Configuration fichier](#configuration-fichier)
    - [Utilisation de variables d'environnement](#utilisation-de-variables-denvironnement)
  - [Utilisation  basique](#utilisation--basique)
  - [Utilisation Gin avancé (Mapping JSON, POST)](#utilisation-gin-avancé-mapping-json-post)
  - [Utilisation MongoDB](#utilisation-mongodb)
  - [Separation Client serveur](#separation-client-serveur)
  - [Tests](#tests)
  - [Helm et Skaffold](#helm-et-skaffold)
    - [Helm](#helm)
    - [Skaffold](#skaffold)

## Agencement des packages et du code GO

A la racine se trouve uniquement les fichiers go.mod, go.sum et main.go

Des packages sont crée pour chaque unité fonctionnelle (regroupement de fonctions/services autours d'un même sujet). On peut avoir plusieurs fichier au sein d'un même package pour bien séparer plusieurs fonctionnalité, par éxemple avoir un fichier qui contient les méthodes/types relatif au handling des appels, puis un autre fichier qui contient les méthodes/types qui font le processing/calcul

Le fichier main.go contient la fonction main et l'ensemble du code utiliser pour initialiser la configuration global et l'initialisation des autres modules

Le dossier charts contient l'ensemble des fichiers requis pour crée un charts Helm



## Configuration Viper
[Source/Documentation sur github](https://github.com/spf13/viper)

La gestion des configuration préparée dans le fichier config/Config.go

### Configuration fichier

Cette configuration est faite dans le fichier [Config.go](/config/Config.go)

```golang
viper.SetConfigType("yaml")
viper.AddConfigPath(dockerConfigPath)
viper.AddConfigPath(localConfigPath)
viper.SetConfigName("config.yaml")
```

Ce bloc de code configure le format du fichier de configuration ainsi que le ou les endroits auxquels Viper ira chercher la configuration

`viper.ReadInConfig()` permet de déclencher la lecture du fichier de configuration

`viper.Unmarshal(&Config)` permet de mapper la configuration dans la structure du type de Config. L'agencement du fichier charger doit correspondre


### Utilisation de variables d'environnement

Toujours dans le fichier [Config.go](/config/Config.go)

`viper.SetDefault("MONGO_HOST", "localhost")` permet de définir une valeur par défaut pour une variable Viper
`viper.BindEnv("MONGO_HOST")` permet de dire à Viper de mapper cette variable à une variable d'environnement

Ces variables pourront ensuite être réutiliser autre part dans le programme en utilisant `viper.GetString("MONGO_HOST")` (par exemple dans le fichier [mongodemo.go L13](mongodemo/mongodemo.go))

## Utilisation  basique

[Source de Gin](https://github.com/gin-gonic/gin)

[Documentation de de Gin](https://pkg.go.dev/github.com/gin-gonic/gin#section-documentation)

Baser sur l'exemple du package calculator

Un routeur Gin par défaut est initialisé dans la fonction `setupRouter` dans le fichier [main.go](main.go)

`r := gin.Default()` permet de crée un routeur Gin contenant par défaut un logger et un panic recover.

`r` est une instance de gin.Engine qui correspond au router, ce routeur permet de définir les routes et configurer le(s) middleware(s). Voir la doc pour plus d'informations

On utilise `r.Group(chemin)` pour crée un sous routeur (type `gin.RouterGroup`), et passer ce sous routeur à une fonction `SetupRoute` qui sera chargée de définir plus précisément les routes relative à un service. Exemple:
```golang
calculator.NewCalculatorHandler().SetupRoutes(r.Group("/calc"))
```

La fonction `SetupRoute` est définie dans le fichier [calculator_handler.go](calculator/calculator_handler.go), cette fonction utilise l'instance `gin.RouterGroupe` pour associer des chemins a des fonctions handlers. La fonction handler doit prendre en paramètre une seul entré de type `*gin.Context`

Exemple 
```golang
func (C CalculatorHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/add", C.handleAdd)
}

func (C CalculatorHandler) handleAdd(c *gin.Context) {
	nums, err := convertParametersToNumber(c, "n")
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, fmt.Sprint(C.C.Add(nums...)))
}
```

Ici, l'objet `router` (type `gin.RouterGroup`) est celui qui nous a été passer depuis le fichier [main.go](main.go) `r.Group("/calc")`, donc la commande `router.GET("/add", C.handleAdd)` asssigne la fonction `handleAdd` à la route `/calc/add`.

Pour récupérer la valeur d'un paramètre d'URL on peux utiliser une de ces 3 fonctions sur l'objet `c *gin.Context`
```golang
Query(key string) string
DefaultQuery(key, defaultValue string) string
GetQuery(key string) (string, bool)
```

Pour récupérer toutes les valeurs qui ont le même nom, on peut utiliser une des fonctions suivante
```golang
QueryArray(key string) []string
GetQueryArray(key string) (string, bool)
```
Dans la fonction `convertParametersToNumber` utilise `GetQueryArray`

Enfin pour envoyer une réponse on peut utiliser la fonction `String(code int, format string, values ...interface{})` qui permet de donner le code de retour http ainsi qu'une chaine de caractère 

[Pour plus d'informations sur les manip avec gin.Context](https://pkg.go.dev/github.com/gin-gonic/gin#Context)

## Utilisation Gin avancé (Mapping JSON, POST)
Baser sur l'exemple du package json_decorator

Pour facilement mapper du json on se sert tags sur les struct ainsi que de méthodes fournie par gin pour mapper le body d'une request sur un type et d'une méthode de l'objet `gin.Context` pour renvoyer un résultat sous forme de json.

Exemple de struct avec des tags pour le binding json
```golang
type Message struct {
	Message string `json:"message" binding:"required"`
}
```
Ici on déclare un type `Message` qui contient une valeur `Message` de type string. Cette valeur `Message` a 2 tags en plus, `json` qui permet d'indiquer à quel clé la valeur sera lié et `required` qui permet d'indiquer si une erreur doit être levée si cette clé est manquante

Pour binder le contenu du body d'une requête POST, `gin.Context` met a disposition la méthode `BindJSON(obj interface{}) error`, et pour renvoyer un objet au format JSON on peut utiliser la méthode `JSON(code int, obj interface{})` qui permet de spécifier un code de retour HTML et l'objet à sérialiser.

Exemple dispo dans [json_decorator_handler.go](json_decorator/json_decorator_handler.go)

## Utilisation MongoDB

Baser sur l'exemple du package mongodemo

[Source et documentation](https://github.com/mongodb/mongo-go-driver)
[Documentation go](https://pkg.go.dev/go.mongodb.org/mongo-driver/mongo)
Pour pouvoir interagir avec une base de donnée MongoDB il faut s'y connecter en utilisant la fonction `mongo.Connect(ctx, options.Client().ApplyURI(url))`. Cette méthode retourne un objet de type `*mongo.Client`

Ce client assure la connexion vers l'instance MongoDB et depuis ce client on peut ensuite récupérer un lien vers une base en utilisant `client.Database(database_name)`. Cette méthode retourne un objet de type `*mongo.Database`

Depuis cette base de donnée on peut ensuite utiliser la méthode `database.Collection(collection_name)` qui retourne un objet de type `*mongo.Collection`

Enfin via cet objet Collection on peut accéder au fonctions de CRUD de 
mongoDB.

Des exemples sont disponible dans [mongodemo.go](mongodemo/mongodemo.go) 


De plus, on peut ajouter des tags a un type pour que mongodb puisse automatiquement mapper les valeurs aux champs d'un document.
Par exemple :
```golang
type MessageBson struct {
	Id      primitive.ObjectID `bson:"_id,omitempty"`
	Titre   string             `bson:"titre,omitempty"`
	Contenu string             `bson:"contenu"`
}
```

Ici, les valeurs Titre et Contenu dispose tout deux de tags pour donner le nom du champ json et en bson, le nom du champ associé étant le même en json et bson.
Le champ id à la particularité qu'en json le nom du champ est `id` et le nom du champ en bson est `_id`, c'est golang qui s'occupera de faire la traduction.

[Documentation du type collection pour les interactions CRUD](https://pkg.go.dev/go.mongodb.org/mongo-driver/mongo#Collection)

## Separation Client serveur

Un exemple de séparation client serveur est disponible dans le package mongodemo

Un sous-module spécifique contenant une interface et des struct de donnée est disponible dans [/mongodemo/mongodemo_client/types](/mongodemo/mongodemo_client/types/types.go). Ainsi qu'une implémentation de l'interface qui permet de transformer les appels sur l'interface en appel HTTP qui seront gérer par le serveur. 

Cette interface est aussi utilisé coté serveur ([mongodemo.go](/mongodemo/mongodemo.go)) pour la définition du service pour simplifier le lien entre le client et le serveur

Il est important de noté que le type `Message` déclaré par le module client est recrée dans l'implémentation coté serveur `MessageBson` et contient  les tags pour le mapping bson ainsi que la variable Id avec le type `primitive.ObjectID`

## Tests

[Les 10 commandements des test unitaires](https://blog.engineering.publicissapient.fr/2008/04/11/les-10-commandements-des-tests-unitaires/)

TL;DR on test les unité de code les plus petites possible

Il faut éviter tout interactions avec des systèmes externes

Par exemple pour le package mongodemo, le code qui interagit directement avec les package mongo sont pas testé car il requièrent une connexion directe avec une instance MongoDB. De ce fait, le code qui interagit avec le package mongo est regroupé derrière une interface qui permet de crée un objet factice qui implémente cette interface et est fournis au autres instance qui en ont besoins. Le code qui interagit avec mongo doit être le plus simple et minimaliste possible pour éviter toute erreur possible, limite en faire un wrapper

C'est ce qui est fait dans [mongodemo_test](mongodemo/mongodemo_test.go) avec le type MockMongoDemo (fin du fichier)

Ce type est compatible avec l'interface `IMongoDemo` et est donc fournis à l'objet `MongoDemoHandler` lorsqu'il est instancié.

## Helm et Skaffold

### Helm

[Documentation de Helm](https://helm.sh/docs/)

Helm permet de simplement regrouper un ensemble de fichier de description Kubernetes (Deployments, Services, Endpoints ect..) et de pouvoir changer leur valeur via des fichiers de configuration ou directement en ligne de commande, ou de déployer plus ou moins de fichier yaml en fonction des paramètres. Le regroupement de ces fichiers est appeler un chart. Un chart peut aussi simplifier l'installation de dépendances (sous forme de charts)

Dans ce projet, le chart déploie 2 fichiers [deployment.yaml](charts/go-template/templates/deployment.yaml) et [service.yaml](charts/go-template/templates/service.yaml)

`deployment.yaml` contient la description du Deployment (l'image a utiliser, nombre de réplica, variables d'environnements ect)

`service.yaml` contient la définition des services nécessaire pour le 
fonctionnement du deployement (notamment la connexion vers mongodb)

Pour  plus d'infos sur Helm:
* [Quickstart (utilisation de helm CLI)](https://helm.sh/docs/intro/quickstart/)
* [Getting started (création de charts)](https://helm.sh/docs/chart_template_guide/getting_started/)

### Skaffold
[Documentation de Skaffold](https://skaffold.dev/docs/)

Skaffold est un outils permettant de simplifier le déploiement de projets en cours de développement.

Il est capable de surveiller le contenu d'un répertoire, et lorsqu'il détecte un changement va générer un build puis va déployer ce build sur un environnement exécution.

Il est  capable de faire le  déploiement dans un cluster local ou dans un cluster distant

Ce projet dispose d'une configuration basique de Skaffold (dans le fichier [skaffold.yaml](skaffold.yaml)).
Cette configuration génère une nouvelle image docker dès qu'un changement est détecté dans le répertoire courant code puis le déployer sur le cluster lié à kubectl

Pour plus d'info sur skaffold:
* [Pipeline stages](https://skaffold.dev/docs/pipeline-stages/)
* [reference yaml](https://skaffold.dev/docs/references/yaml/)