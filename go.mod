module gitlab.com/irt-sb1/go-template

go 1.16

require (
	github.com/gin-gonic/gin v1.7.0
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.5.1
	gitlab.com/irt-sb1/go-template/mongodemo/mongodemo_client v0.0.0-00010101000000-000000000000
)

replace gitlab.com/irt-sb1/go-template/mongodemo/mongodemo_client => ./mongodemo/mongodemo_client
