package main

import (
	"fmt"
	"testing"
)

func TestRoutesSetup(t *testing.T) {
	r := setupRouter()
	routesToMatch := []string{"/calc/add", "/calc/sub", "/calc/mul", "/calc/div", "/json/decorate", "/json/extract"}
	fmt.Print(r.Routes())
	matchedRoute := 0
	for _, v := range r.Routes() {
		for _, match := range routesToMatch {
			if v.Path == match {
				matchedRoute++
			}
		}
	}
	if matchedRoute != 6 {
		t.Error("Error during route matching")
	}
}
