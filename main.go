package main

import (
	"log"

	"gitlab.com/irt-sb1/go-template/calculator"
	"gitlab.com/irt-sb1/go-template/config"
	"gitlab.com/irt-sb1/go-template/json_decorator"
	"gitlab.com/irt-sb1/go-template/mongodemo"

	"github.com/gin-gonic/gin"
)

func main() {
	config.Load()
	r := setupRouter()

	if err := r.Run(config.Config.Server.ListenAddress); err != nil {
		log.Println(err)
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	calculator.NewCalculatorHandler().SetupRoutes(r.Group("/calc"))
	json_decorator.JSONDecoratorHandler{}.SetupRoutes(r.Group("/json"))
	mongodemo.NewMongoDemoHandler().SetupRoutes(r.Group("/mongo"))
	return r
}
